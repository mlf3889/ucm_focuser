﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UCM_Focuser
{
    public partial class Form1 : Form
    {

        private bool nonNumberEntered = false;
        public Form1()
        {
            InitializeComponent();
            System.ComponentModel.IContainer components = new System.ComponentModel.Container();
            serialPort1 = new System.IO.Ports.SerialPort(components);
            serialPort1.PortName = "COM6";
            serialPort1.BaudRate = 9600;
            serialPort1.DtrEnable = true;
            serialPort1.Open();
        }

        private void grossOut_Click(object sender, EventArgs e)
        {
            serialPort1.Write(new byte[] { Convert.ToByte("4") }, 0, 1);
        }

        private void fineOut_Click(object sender, EventArgs e)
        {
            serialPort1.Write(new byte[] { Convert.ToByte("3") }, 0, 1);
        }

        private void fineIn_Click(object sender, EventArgs e)
        {
            serialPort1.Write(new byte[] { Convert.ToByte("2") }, 0, 1);
        }

        private void grossIn_Click(object sender, EventArgs e)
        {
            serialPort1.Write(new byte[] { Convert.ToByte("1") }, 0, 1);
        }


        private void move_out(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            // Initialize the flag to false.
            nonNumberEntered = false;
            textBox1.Text = e.KeyCode.ToString();

            if (Keys.Right) {
                textBox1.Text = "go west young man";
            }

            // Determine whether the keystroke is a number from the top of the keyboard.
            if (e.KeyCode < Keys.D0 || e.KeyCode > Keys.D9)
            {
                // Determine whether the keystroke is a number from the keypad.
                if (e.KeyCode < Keys.NumPad0 || e.KeyCode > Keys.NumPad9)
                {
                    // Determine whether the keystroke is a backspace.
                    if (e.KeyCode != Keys.Back)
                    {
                        // A non-numerical keystroke was pressed.
                        // Set the flag to true and evaluate in KeyPress event.
                        nonNumberEntered = true;
                    }
                }
            }
        }




    }
}
