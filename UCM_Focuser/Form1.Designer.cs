﻿namespace UCM_Focuser
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grossOut = new System.Windows.Forms.Button();
            this.fineOut = new System.Windows.Forms.Button();
            this.fineIn = new System.Windows.Forms.Button();
            this.grossIn = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // grossOut
            // 
            this.grossOut.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grossOut.Location = new System.Drawing.Point(12, 78);
            this.grossOut.Name = "grossOut";
            this.grossOut.Size = new System.Drawing.Size(81, 30);
            this.grossOut.TabIndex = 0;
            this.grossOut.Text = "<<";
            this.grossOut.UseVisualStyleBackColor = true;
            this.grossOut.Click += new System.EventHandler(this.grossOut_Click);
            // 
            // fineOut
            // 
            this.fineOut.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fineOut.Location = new System.Drawing.Point(48, 114);
            this.fineOut.Name = "fineOut";
            this.fineOut.Size = new System.Drawing.Size(81, 30);
            this.fineOut.TabIndex = 1;
            this.fineOut.Text = "<";
            this.fineOut.UseVisualStyleBackColor = true;
            this.fineOut.Click += new System.EventHandler(this.fineOut_Click);
            // 
            // fineIn
            // 
            this.fineIn.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fineIn.Location = new System.Drawing.Point(135, 114);
            this.fineIn.Name = "fineIn";
            this.fineIn.Size = new System.Drawing.Size(81, 30);
            this.fineIn.TabIndex = 2;
            this.fineIn.Text = ">";
            this.fineIn.UseVisualStyleBackColor = true;
            this.fineIn.Click += new System.EventHandler(this.fineIn_Click);
            // 
            // grossIn
            // 
            this.grossIn.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grossIn.Location = new System.Drawing.Point(174, 150);
            this.grossIn.Name = "grossIn";
            this.grossIn.Size = new System.Drawing.Size(81, 30);
            this.grossIn.TabIndex = 3;
            this.grossIn.Text = ">>";
            this.grossIn.UseVisualStyleBackColor = true;
            this.grossIn.Click += new System.EventHandler(this.grossIn_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(135, 46);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.grossIn);
            this.Controls.Add(this.fineIn);
            this.Controls.Add(this.fineOut);
            this.Controls.Add(this.grossOut);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button grossOut;
        private System.Windows.Forms.Button fineOut;
        private System.Windows.Forms.Button fineIn;
        private System.Windows.Forms.Button grossIn;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox textBox1;
    }
}

